with Ada.Text_IO; use Ada.Text_IO;
with Regexp_Builders.Core;
with Regexp_Builders.Fragments;
with Gnat.Regpat;

procedure Main is
   use Regexp_Builders, Regexp_Builders.Core;

   Sign : constant Regexp_Pattern := Any_Char_From ("+-");

   Int_Pattern : constant Regexp_Pattern :=
     At_Least_One (Digit);

   Exponent : constant Regexp_Pattern :=
     Any_Char_From ("eE")  & Maybe (Sign) & Int_Pattern;

   Float_Regexp : constant Regexp_Pattern :=
     Maybe (Sign) & Int_Pattern & "." & Int_Pattern & Maybe (Exponent);

   Basic_Integer : constant Regexp_Pattern :=
     List_Of (Int_Pattern, "_", Allow_Space => None);

   Ada_Integer  : constant Regexp_Pattern :=
     Basic_Integer or (Int_Pattern & "#" & Basic_Integer & "#");

   Ada_Identifier : constant Regexp_Pattern :=
     List_Of (At_Least_One (Alnum), "_", Allow_Space => None);

begin
   Put_Line (Print (Float_Regexp));
   Put_Line (Print (Ada_Integer));
   Put_Line (Print (Digit or Upper or Interval ('@', '^')));
   Put_Line (Print (Ada_Identifier));

   declare
      use Fragments;
      use Gnat.Regpat;


      Integer_Pair : constant Regexp_Pattern :=
        Fragment(Ada_Integer, Label("re"))
        & Any(Whitespace)
        & ","
        & Any(Whitespace)
        & Fragment(Ada_Integer, Label("im"));

      G : Fragments.Group_Map;

      R : constant Pattern_Matcher := Compile(Print(Integer_Pair));
      M : Match_Array(0..2);
      Test : constant String := "pippo 2#100_001#,   42 zorro";

      function Get(X : String; Pos:Match_Location) return String
      is (" <" & X(Pos.First..Pos.Last) & ">");

      function Get(X : String;
                   Pos:Match_Array;
                   G: Group_Map;
                   L : String)
                   return String
      is (Get(X, Pos(Integer(G(Label(L))))));
   begin
      Put_Line (Print (Integer_Pair, G));

      for Pos in G.Iterate loop
         Put_Line(Image(Label(Pos)) & "->" & Group(Pos)'Image);
      end loop;

      if Match(R, "2#100_001#,42") then
         Put_Line("OK");
      else
         Put_Line("NO");
      end if;

      Match(R, Test, M);

      if M(0) /= No_Match then
         Put_Line("OK" & Get(Test, M(0)));
         Put_Line("Im =" & Get(Test, M, G, "im"));
         Put_Line("Re =" & Get(Test, M, G, "re"));
      else
         Put_Line("NO");
      end if;

   end;

   declare
      use Gnat.Regpat;
      type Names is (Real, Imag);

      package Named_Fragments is
        new Symbolic_Labels(Names);

      Name_T : Named_Fragments.Name_Map;

      Complex_Re : constant Regexp_Pattern :=
        +"("
        & Named_Fragments.Fragment(Float_Regexp, Real)
        & Any(Whitespace)
        & Text(",")
        & Any(Whitespace)
        & Named_Fragments.Fragment(Float_Regexp, Imag)
        & Text(")");

      R : constant Pattern_Matcher :=
        Compile(Named_Fragments.Print(Complex_Re, Name_T));

      M : Match_Array(0..2);
      Test : constant String := "33 + (3.14, 42.5e-6) * 0.9";

      function Get(X : String; Pos:Match_Location) return String
      is (" <" & X(Pos.First..Pos.Last) & ">");

   begin
      Match(R, Test, M);

      if M(0) /= No_Match then
         Put_Line("OK" & Get(Test, M(0)));
         Put_Line("Im =" & Get(Test, M(Name_T(Imag))));
         Put_Line("Re =" & Get(Test, M(Name_T(Real))));
      else
         Put_Line("NO");
      end if;
   end;
end Main;

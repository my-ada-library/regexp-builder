pragma Ada_2012;
with Ada.Containers.Indefinite_Vectors;
with Ada.Strings.Fixed;
with Ada.Strings.Maps.Constants;   use Ada.Strings.Maps.Constants;
with Ada.Strings.Unbounded;        use Ada.Strings.Unbounded;

pragma Warnings (Off, "no entities of ""Ada.Text_IO"" are referenced");
with Ada.Text_IO;                  use Ada.Text_IO;

with Regexp_Builders.Builtin_Formatters;   use Regexp_Builders.Builtin_Formatters;
with Regexp_Builders.Formatters;           use Regexp_Builders.Formatters;
with Regexp_Builders.Labeled_Strings;      use Regexp_Builders.Labeled_Strings;

package body Regexp_Builders.Core is
   use Pattern_Trees;

   type Syntax_Role is (Union, Product, Basic_Regexp, Elementary_Regexp, Anchored);



   type Regexp_Image is
      record
         Image   : Labeled_String;
         Role    : Syntax_Role;
      end record;

   function Head (X : Regexp_Pattern) return Atom
   is (First_Child_Element (X.T.Root));


   function Binary (X, Y : Regexp_Pattern; Op : Binary_Operator)
                    return Regexp_Pattern
     with Post => Head (Binary'Result).Op = Op;



   function New_Fragment (X     : Regexp_Pattern;
                          Label : Fragments.Fragment_Label)
                          return Regexp_Pattern
     with Post => Head (New_Fragment'Result).Op = Fragment;


   function Unary (X      : Regexp_Pattern;
                   Op     : Unary_Operator;
                   Greedy : Boolean)
                   return Regexp_Pattern
     with Post => Head (Unary'Result).Op = Op;


   --  function Quote (X     : String;
   --                  Op    : Leaf_Operator;
   --                  Style : Regexp_Style) return String
   --  is (X);
   --
   --  pragma Compile_Time_Warning (True, "Quote is a placeholder");
   --
   --  -----------------
   --  -- N_Fragments --
   --  -----------------
   --
   --  function N_Fragments (X : Regexp_Pattern) return Natural
   --  is
   --     Count : Natural := 0;
   --  begin
   --     for Node in Iterate_Subtree (X.T.Root) loop
   --        if Element (Node).Op = Fragment then
   --           Count := Count + 1;
   --        end if;
   --     end loop;
   --
   --     return Count;
   --  end N_Fragments;

   --
   --  function Max_Fragment_Index (X : Regexp_Pattern) return Natural
   --  is
   --     Result : Natural := 0;
   --  begin
   --     for Node in Iterate_Subtree (X.T.Root) loop
   --        if Element (Node).Op = Fragment then
   --           Result := Natural'Max(Result, Element(Node).Label);
   --        end if;
   --     end loop;
   --
   --     return Result;
   --  end Max_Fragment_Index;

   -----------
   -- Print --
   -----------

   function Print
     (X : Regexp_Pattern; Style : Regexp_Style := Gnat) return String
   is
      Ignored : Fragments.Group_Map;
   begin
      return Print (X, Ignored, Style);
   end Print;

   function To_Regexp_Image  (X          : Cursor;
                              Formatter  : Abstract_Formatter'Class)
                              return Regexp_Image
   is

      package Image_Vectors is
        new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                               Element_Type => Regexp_Image);
      --  use String_Vectors;
      subtype Image_Vector is Image_Vectors.Vector;

      function Print_Children (X : Cursor) return Image_Vector
      is
         Result : Image_Vector;

         procedure Process (Position : Cursor) is
         begin
            Result.Append (To_Regexp_Image (Position, Formatter));
         end Process;
      begin
         Iterate_Children (X, Process'Access);

         return Result;
      end Print_Children;

      function To_Image (Image : Labeled_String; Role : Syntax_Role)
                         return Regexp_Image
      is (Regexp_Image'(Image, Role));


      function Precedence (X         : Regexp_Image;
                           New_Role  : Syntax_Role;
                           Formatter : Abstract_Formatter'Class)
                           return Labeled_String
      is (if X.Role in Basic_Regexp .. Elementary_Regexp or New_Role < X.Role then
             X.Image
          else
             Formatter.Non_Capturing_Group (X.Image)); -- non capturing parenthesis

      Children : Image_Vector;
      Buffer : Labeled_String;
   begin
      case Element (X).Op is
         when Anchor =>
            Children := Print_Children (X);

            if Children.Length /= 1 then
               raise Program_Error;
            end if;

            case Element (X).Position is
               when At_Beginning =>
                  return To_Image ("^" & Children.First_Element.Image, Anchored);

               when At_End =>
                  return To_Image (Children.First_Element.Image & "$", Anchored);

               when Both =>
                  return To_Image ("^" & Children.First_Element.Image & "$", Anchored);

            end case;
         when Concat =>
            Children := Print_Children (X);


            Buffer := Empty_Labelled_String;

            for El of Children loop
               Buffer := Buffer & Precedence (El, Product, Formatter);
            end loop;

            return To_Image (Buffer, Product);

         when Alternative =>
            Children := Print_Children (X);

            Buffer := Empty_Labelled_String;

            for El of Children loop
               Buffer := Buffer & "|" & Precedence (El, Union, Formatter);
            end loop;

            -- Remove the spurious "|" at the beginning
            return To_Image (Delete_First (Buffer), Union);

         when Any =>
            Children := Print_Children (X);

            if Children.Length /= 1 then
               raise Program_Error;
            end if;

            declare
               Adjusted : constant Labeled_String :=
                 Precedence (X         => Children.First_Element,
                             New_Role  => Basic_Regexp,
                             Formatter => Formatter);

               Formatted : constant Labeled_String :=
                 Formatter.Any (Adjusted, Element (X).Greedy);
            begin
               return To_Image (Formatted, Basic_Regexp);
            end;

         when At_Least_One =>
            Children := Print_Children (X);

            if Children.Length /= 1 then
               raise Program_Error;
            end if;


            declare
               Adjusted : constant Labeled_String :=
                 Precedence (X         => Children.First_Element,
                             New_Role  => Basic_Regexp,
                             Formatter => Formatter);

               Formatted : constant Labeled_String :=
                 Formatter.At_Least_One (Adjusted, Element (X).Greedy);
            begin
               return To_Image (Formatted, Basic_Regexp);
            end;
         when Maybe =>
            Children := Print_Children (X);

            if Children.Length /= 1 then
               raise Program_Error;
            end if;

            return
              To_Image
                (Formatter.Optional
                   (Precedence (Children.First_Element, Basic_Regexp, Formatter)),
                 Basic_Regexp);

         when Repetition =>
            Children := Print_Children (X);

            if Children.Length /= 1 then
               raise Program_Error;
            end if;

            declare
               Formatted : constant Labeled_String :=
                 Formatter.Repetition
                   (Argument => Precedence (X => Children.First_Element,
                                            New_Role  => Basic_Regexp,
                                            Formatter => Formatter),
                    Min      => Element (X).Min,
                    Max      => Element (X).Max);
            begin
               return To_Image (Formatted, Basic_Regexp);
            end;

         when Fragment =>
            Children := Print_Children (X);

            if Children.Length /= 1 then
               raise Program_Error;
            end if;

            declare
               Formatted : constant Labeled_String :=
                 Formatter.Capturing_Group (Children.First_Element.Image,
                                            Element(X).Label);
            begin
               return To_Image (Formatted, Elementary_Regexp);
            end;

         when Charset =>
            return
              To_Image (Formatter.Charset (Element (X).Val), Elementary_Regexp);

         when Text =>
            return
              To_Image (Formatter.Text (Element (X).Val), Elementary_Regexp);

         when Any_Char =>
            return To_Image (To_Labeled("."), Elementary_Regexp);

         when Whitespace =>
            return To_Image (Formatter.Whitespace, Elementary_Regexp);

      end case;
   end To_Regexp_Image;


   function Print (X         : Regexp_Pattern;
                   Labels    : out Fragments.Group_Map;
                   Formatter : Regexp_Formatter) return String
   is
      Root : constant Cursor :=First_Child (X.T.Root);
      Result : constant Labeled_String :=
        To_Regexp_Image (Root, Formatter.all).Image;
   begin
      Labels := Get_Group_Map(Result);
      return To_String (Result);
   end Print;

   function Print (X      : Regexp_Pattern;
                   Labels : out Fragments.Group_Map;
                   Style  : Regexp_Style := Gnat) return String
   is
      Root : constant Cursor :=First_Child (X.T.Root);
      Result : constant Labeled_String :=
        To_Regexp_Image(Root, Formatter_For(Style)).Image;
   begin
      Labels := Get_Group_Map(Result);
      return To_String(Result);
   end Print;

   function Is_Final (X : Regexp_Pattern) return Boolean
   is (First_Child_Element (X.T.Root).Op = Anchor);


   -------
   -- P --
   -------

   function Text (X : String) return Regexp_Pattern is
      Result : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      Result.Append_Child (Parent   => Result.Root,
                           New_Item => Atom'(Op     => Text,
                                             Length => X'Length,
                                             Val    => X));

      return Regexp_Pattern'(T => Result);
   end Text;

   --------------
   -- Any_Char --
   --------------

   function Any_Char return Regexp_Pattern
   is
      Result : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      Result.Append_Child (Parent   => Result.Root,
                           New_Item => Atom'(Op     => Any_Char,
                                             Length => 0,
                                             Val    => ""));

      return Regexp_Pattern'(T => Result);
   end Any_Char;

   -------
   -- P --
   -------

   function Charset (X : Character_Set) return Regexp_Pattern is
      T      : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
      Chars  : constant String := To_Sequence (X);
   begin
      T.Append_Child (Parent   => T.Root,
                      New_Item => Atom'(Op     => Charset,
                                        Length => Chars'Length,
                                        Val    => Chars));

      --  Put_Line (Pattern_Trees.Child_Count (T.Root)'Image);

      return Regexp_Pattern'(T => T);
   end Charset;

   function Interval (Lo, Hi : Character) return Regexp_Pattern
   is (Charset (To_Set (Character_Range'(Low  => Lo, High => Hi)))) ;

   function Whitespace return Regexp_Pattern is
      T      : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      T.Append_Child (Parent   => T.Root,
                      New_Item => Atom'(Op     => Whitespace,
                                        Length => 0));

      return Regexp_Pattern'(T => T);

   end Whitespace;

   function New_Head (Op     : True_Operator;
                      Greedy : Boolean := True) return Atom
   is (case Op is
          when At_Least_One      => (Op => At_Least_One, Length => 0, Greedy => Greedy),
          when Maybe             => (Op => Maybe, Length => 0, Greedy => Greedy),
          when Any               => (Op => Any, Length => 0, Greedy => Greedy),
          when Concat            => (Op => Concat, Length => 0),
          when Alternative       => (Op => Alternative, Length => 0));

   function Binary (X, Y : Regexp_Pattern; Op : Binary_Operator)
                    return Regexp_Pattern
   is
      Result : Pattern_Trees.Tree;
      Pos    : Cursor;
   begin
      if Head (X).Op = Op then

         Result := X.T;

         Result.Copy_Subtree (Parent => First_Child (Result.Root),
                              Before => No_Element,
                              Source => First_Child (Y.T.Root));

      elsif Head (Y).Op = Op then

         Result := Y.T;

         Pos := First_Child (Result.Root);
         Result.Copy_Subtree (Parent => Pos,
                              Before => First_Child (Pos),
                              Source => First_Child (Y.T.Root));
      else

         Result := Empty_Tree;
         Result.Append_Child (Parent   => Result.Root,
                              New_Item => New_Head (Op));


         Result.Copy_Subtree (Parent => First_Child (Result.Root),
                              Before => No_Element,
                              Source => First_Child (X.T.Root));


         Result.Copy_Subtree (Parent => First_Child (Result.Root),
                              Before => No_Element,
                              Source => First_Child (Y.T.Root));


      end if;

      return Regexp_Pattern'(T => Result);
   end Binary;

   ---------
   -- "&" --
   ---------

   function "&" (X, Y : Regexp_Pattern) return Regexp_Pattern
   is (Binary (X, Y, Concat));


   function "&" (X : Regexp_Pattern; Y : String) return Regexp_Pattern
   is (X & Text (Y));

   function "&" (X : String; Y : Regexp_Pattern) return Regexp_Pattern
   is (Text (X) & Y);

   ----------
   -- "or" --
   ----------

   function "or" (X, Y : Regexp_Pattern) return Regexp_Pattern
   is
   begin
      if Head (X).Op = Charset and Head (Y).Op = Charset then
         return Any_Char_From (Head (X).Val & Head (Y).Val);
      else
         return Binary (X, Y, Alternative);
      end if;
   end "or";


   --------------------
   -- Unary_Operator --
   --------------------

   function Unary (X      : Regexp_Pattern;
                   Op     : Unary_Operator;
                   Greedy : Boolean)
                   return Regexp_Pattern
   is
      Result : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      if Head (X).Op in Unary_Operator then
         raise Constraint_Error with "Quantifiers cannot be repeated";
      end if;

      Result.Append_Child (Parent   => Result.Root,
                           New_Item => New_Head (Op, Greedy));

      Result.Copy_Subtree (Parent => First_Child (Result.Root),
                           Before => No_Element,
                           Source => First_Child (X.T.Root));

      return Regexp_Pattern'(T => Result);
   end Unary;

   function Repetition (X   : Regexp_Pattern;
                        Min : Finite_Repetition_Counter;
                        Max : Repetition_Counter)
                        return Regexp_Pattern
   is
      Result : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      if Head (X).Op in Unary_Operator then
         raise Constraint_Error with "Quantifiers cannot be repeated";
      end if;

      Result.Append_Child (Parent   => Result.Root,
                           New_Item => Atom'(Op     => Repetition,
                                             Length => 0,
                                             Min    => Min,
                                             Max    => Max));

      Result.Copy_Subtree (Parent => First_Child (Result.Root),
                           Before => No_Element,
                           Source => First_Child (X.T.Root));

      return Regexp_Pattern'(T => Result);

   end Repetition;

   function New_Fragment (X     : Regexp_Pattern;
                          Label : Fragments.Fragment_Label)
                          return Regexp_Pattern
   is
      Result : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      Result.Append_Child (Parent   => Result.Root,
                           New_Item => Atom'(Op     => Fragment,
                                             Length => 0,
                                             Label  => Label));

      Result.Copy_Subtree (Parent => First_Child (Result.Root),
                           Before => No_Element,
                           Source => First_Child(X.T.Root));

      return Regexp_Pattern'(T => Result);
   end New_Fragment;

   ---------
   -- Any --
   ---------

   function Any (X      : Regexp_Pattern;
                 Greedy : Boolean := True) return Regexp_Pattern
   is (Unary (X, Any, Greedy));

   ------------------
   -- At_Least_One --
   ------------------

   function At_Least_One
     (X      : Regexp_Pattern;
      Greedy : Boolean := True) return Regexp_Pattern
   is (Unary (X, At_Least_One, Greedy));


   -----------
   -- Maybe --
   -----------

   function Maybe
     (X      : Regexp_Pattern;
      Greedy : Boolean := True) return Regexp_Pattern
   is (Unary (X, Maybe, Greedy));


   --------------
   -- Fragment --
   --------------

   function Fragment (X : Regexp_Pattern) return Regexp_Pattern
   is
   begin
      return New_Fragment (X, Fragments.No_Label);
   end Fragment;

   function Fragment (X     : Regexp_Pattern;
                      Label : Fragments.Fragment_Label) return Regexp_Pattern
   is
   begin
      return New_Fragment (X, Label);
   end Fragment;

   ------------
   -- Anchor --
   ------------

   function Anchor (What  : Regexp_Pattern;
                    Where : Anchor_Position)
                    return Regexp_Pattern
   is
      Result : Pattern_Trees.Tree := Pattern_Trees.Empty_Tree;
   begin
      Result.Append_Child (Parent   => Result.Root,
                           New_Item => Atom'(Op       => Anchor,
                                             Length   => 0,
                                             Position => Where));

      Result.Copy_Subtree (Parent => First_Child (Result.Root),
                           Before => No_Element,
                           Source => First_Child (What.T.Root));

      return Regexp_Pattern'(T => Result);
   end Anchor;


   function Any_String return Regexp_Pattern
   is (Any (Any_Char));



   function List_Of (Item        : Regexp_Pattern;
                     Separator   : String;
                     Allow_Space : Side_Type)
                     return Regexp_Pattern
   is
   begin
      case Allow_Space is
         when None   =>
            return List_Of (Item, Text (Separator));

         when Before =>
            return List_Of (Item, Any_Space & Text (Separator));

         when After  =>
            return List_Of (Item, Text (Separator) & Any_Space);

         when Both   =>
            return List_Of (Item, Any_Space & Text (Separator) & Any_Space);
      end case;
   end List_Of;

   function List_Of (Item      : Regexp_Pattern;
                     Separator : Regexp_Pattern)
                     return Regexp_Pattern
   is
   begin
      return Item & Any (Separator & Item);
   end List_Of;


   function Any_Space return Regexp_Pattern
   is (Any (Whitespace));

   function Alpha return Regexp_Pattern
   is (Lower or Upper);

   function Alnum return Regexp_Pattern
   is (Alpha or Digit);

   function Lower return Regexp_Pattern
   is (Interval('a', 'z'));

   function Upper return Regexp_Pattern
   is (Interval('A', 'Z'));

   function Digit return Regexp_Pattern
   is (Charset (Decimal_Digit_Set));

   function Hexadecimal_Digit return Regexp_Pattern
   is (Charset (Hexadecimal_Digit_Set));

   function Word  return Regexp_Pattern
   is (Alnum or Charset (To_Set ("_")));


   package body Symbolic_Labels is
      function To_Fragment_Label (X : Label_Name) return Fragments.Fragment_Label
      is (Fragments.Label(Label_Name'Image(X)));

      procedure To_Name_Map(Item : out Name_Map;
                            Input : Fragments.Group_Map)
      is
      begin
         for L in Label_Name loop
            Item (L) := Input (To_Fragment_Label (L));
         end loop;
      end To_Name_Map;

      function Print (X         : Regexp_Pattern;
                      Labels    : out Name_Map;
                      Formatter : Regexp_Formatter) return String
      is
         First      : constant Integer := Label_Name'Pos (Label_Name'First);
         Last       : constant Integer := Label_Name'Pos (Label_Name'Last);
         Aux_Labels : Fragments.Group_Map;

         Result     : constant String := Print (X, Aux_Labels, Formatter);
      begin
         To_Name_Map(Item  => Labels,
                     Input => Aux_Labels);

         return Result;
      end Print;

      function Print (X      : Regexp_Pattern;
                      Labels : out Name_Map;
                      Style  : Regexp_Style := Gnat) return String
      is
         Root    : constant Cursor := First_Child(X.T.Root);
         Result  : constant Labeled_String :=
           To_Regexp_Image (Root, Formatter_For (Style)).Image;
      begin
         To_Name_Map(Item  => Labels,
                     Input => Get_Group_Map(Result));

         return To_String(Result);
      end Print;

      function Fragment (X     : Regexp_Pattern;
                         Label : Label_Name) return Regexp_Pattern
      is (Fragment (X, To_Fragment_Label (Label)));



   end Symbolic_Labels;
end Regexp_Builders.Core;

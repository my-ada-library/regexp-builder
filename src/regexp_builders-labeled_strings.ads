with Ada.Containers.Vectors;
with Regexp_Builders.Fragments;

private package Regexp_Builders.Labeled_Strings is
   type Labeled_String is private;

   Empty_Labelled_String : constant Labeled_String;

   function Delete_First (X: Labeled_String) return Labeled_String;

   function To_Labeled(X:String) return Labeled_String;

   function To_String(X:Labeled_String) return String;

   function "&"(X : String; Y:Labeled_String) return Labeled_String;

   function "&"(X : Labeled_String; Y:String) return Labeled_String;

   function "&"(X, Y : Labeled_String) return Labeled_String;

   procedure To_Fragment
     (X     : in out Labeled_String;
      Label : Fragments.Fragment_Label := Fragments.No_Label);

   function Get_Group_Map (X : Labeled_String) return Fragments.Group_Map;
private
   type Labelled_Character (Is_Begin_Group : Boolean := False) is
      record
         Char : Character;

         case Is_Begin_Group is
            when True =>
               Label : Fragments.Fragment_Label;

            when False=>
               null;
         end case;
      end record;

   package Unbounded_Labelled_Strings is
     new Ada.Containers.Vectors(Index_Type   => Positive,
                                Element_Type => Labelled_Character);

   type Labeled_String is
      record
         S : Unbounded_Labelled_Strings.Vector;
      end record;

   Empty_Labelled_String : constant Labeled_String :=
     (S=>Unbounded_Labelled_Strings.Empty_Vector);

end Regexp_Builders.Labeled_Strings;

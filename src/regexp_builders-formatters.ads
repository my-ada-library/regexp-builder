with Regexp_Builders.Labeled_Strings;   use Regexp_Builders.Labeled_Strings;
with Regexp_Builders.Fragments;

private package Regexp_Builders.Formatters is

   type Abstract_Formatter is interface;

   function Any
     (Formatter : Abstract_Formatter;
      Argument  : Labeled_String;
      Greedy    : Boolean)
      return Labeled_String is abstract;

   function At_Least_One
     (Formatter : Abstract_Formatter;
      Argument  : Labeled_String;
      Greedy    : Boolean)
      return Labeled_String is abstract;

   function Optional
     (Formatter : Abstract_Formatter;
      Argument  : Labeled_String)
      return Labeled_String is abstract;

   function Repetition
     (Formatter : Abstract_Formatter;
      Argument  : Labeled_String;
      Min       : Finite_Repetition_Counter;
      Max       : Repetition_Counter)
      return Labeled_String is abstract;

   function Charset
     (Formatter : Abstract_Formatter;
      Argument  : String)
      return Labeled_String is abstract;

   function Non_Capturing_Group
     (Formatter : Abstract_Formatter;
      Argument  : Labeled_String)
      return Labeled_String is abstract;

   function Capturing_Group
     (Formatter : Abstract_Formatter;
      Argument  : Labeled_String;
      Fragment  : Fragments.Fragment_Label)
      return Labeled_String is abstract;

   function Text
     (Formatter : Abstract_Formatter;
      Argument  : String)
      return Labeled_String is abstract;

   function Whitespace
     (Formatter : Abstract_Formatter)
      return Labeled_String is abstract;

end Regexp_Builders.Formatters;

pragma Ada_2012;
with Regexp_Builders.Fragments;

package body Regexp_Builders.Labeled_Strings is
   procedure To_Fragment
     (X     : in out Labeled_String;
      Label : Fragments.Fragment_Label := Fragments.No_Label)
   is
      First : constant Positive := X.S.First_Index;
      C : constant Character := X.S(First).Char;
   begin
      X.S(First) := Labelled_Character'(Is_Begin_Group => True,
                                        Char           => C,
                                        Label          => Label);
   end To_Fragment;

   function Get_Group_Map (X : Labeled_String) return Fragments.Group_Map
   is
      use type Fragments.Fragment_Label;
      use type Fragments.Group_Index;

      Result : Fragments.Group_Map;
      Group_Counter : Fragments.Group_Index := Fragments.Group_Index'First;
   begin
      for C of X.S loop
         if C.Is_Begin_Group then
            if C.Label /= Fragments.No_Label then

               if Result.Contains (C.Label) then
                  raise Constraint_Error
                    with "Duplicated label '" & Fragments.Image(C.Label) & "'";
               end if;

               Result.Insert(Label => C.Label,
                             Group => Group_Counter);
            end if;

            Group_Counter := Group_Counter+1;
         end if;
      end loop;

      return Result;
   end Get_Group_Map;

   ----------------
   -- To_Labeled --
   ----------------

   function To_Labeled(X:String) return Labeled_String
   is
      Result : Labeled_String;
   begin
      for Idx in X'Range loop
         Result.S.Append(Labelled_Character'(Char           => X(Idx),
                                           Is_Begin_Group => False));
      end loop;

      return Result;
   end To_Labeled;

   ---------------
   -- To_String --
   ---------------

   function To_String (X : Labeled_String) return String is
      Result : String(X.S.First_Index..X.S.Last_Index);
   begin
      for Idx in Result'Range loop
         Result(Idx) := X.s(Idx).Char;
      end loop;

      return Result;
   end To_String;

   ---------
   -- "&" --
   ---------
   function "&"(X : String; Y:Labeled_String) return Labeled_String
   is
      use type Unbounded_Labelled_Strings.Vector;
   begin
      return (S => To_Labeled(X).S & Y.S);
   end "&";

   ---------
   -- "&" --
   ---------

   function "&"(X : Labeled_String; Y:String) return Labeled_String
   is
      use type Unbounded_Labelled_Strings.Vector;
   begin
      return (S => X.S & To_Labeled(Y).S);
   end "&";


   ---------
   -- "&" --
   ---------

   function "&"(X, Y : Labeled_String) return Labeled_String
   is
      use Unbounded_Labelled_Strings;
   begin
        return (S=> X.S & Y.S);
   end "&";
   ------------------
   -- Delete_First --
   ------------------

   function Delete_First (X: Labeled_String) return Labeled_String
   is
      use Unbounded_Labelled_Strings;

      Tmp : Vector := X.S;
   begin
      Tmp.Delete_First;

      return (S=> Tmp);
   end Delete_First;




end Regexp_Builders.Labeled_Strings;

with Regexp_Builders.Formatters;

private package Regexp_Builders.Builtin_Formatters is
   function Formatter_For (Style : Regexp_Style)
                              return Formatters.Abstract_Formatter'Class;

   Unimplemented_Builtin_Formatter : exception;

end Regexp_Builders.Builtin_Formatters;

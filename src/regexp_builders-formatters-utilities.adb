with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
with Partial_Order_Sorting.Array_Sort;

package body Regexp_Builders.Formatters.Utilities is

   -----------
   -- Split --
   -----------

   procedure Split (Item            : Character_Set;
                    Metacharacters  : Character_Set;
                    Meta_Subset     : out Character_Set;
                    Non_Meta_Subset : out Character_Set)
   is
   begin
      Meta_Subset := Item and Metacharacters;
      Non_Meta_Subset := Item and not Metacharacters;
   end Split;


   -----------
   -- Split --
   -----------

   procedure Split (Item            : String;
                    Metacharacters  : String;
                    Meta_Subset     : out Character_Set;
                    Non_Meta_Subset : out Character_Set)
   is
   begin
      Split (Item            => To_Set (Item),
             Metacharacters  => To_Set (Metacharacters),
             Meta_Subset     => Meta_Subset,
             Non_Meta_Subset => Non_Meta_Subset);
   end Split;

   ---------------
   -- Quote_Set --
   ---------------

   function Quote_Set
     (Item    : Character_Set;
      Process : access function (X : Character) return String)
      return String
   is
      Result : Unbounded_String;
   begin
      for Ch of To_Sequence (Item) loop
         Result := Result & Process (Ch);
      end loop;

      return To_String (Result);
   end Quote_Set;

   ---------------
   -- Quote_Set --
   ---------------

   function Quote_Set
     (Item   : Character_Set;
      Prefix : String)
      return String
   is
      function Process (X : Character) return String
      is (Prefix & X);
   begin
      return Quote_Set (Item, Process'Access);
   end Quote_Set;



   function Quote_Metacharacters
     (Item           : String;
      Metacharacters : String)
      return String
   is
      Item_Set : constant Character_Set := To_Set (Item);
      Meta_Set : constant Character_Set := To_Set (Metacharacters);
      Meta     : constant String := To_Sequence (Item_Set and Meta_Set);
      Normal   : constant String := To_Sequence (Item_Set and not Meta_Set);
      Quoted   : String (1 .. 2 * Meta'Length);

      Cursor : Positive := Quoted'First;
   begin
      for C of Meta loop
         Quoted (Cursor .. Cursor + 1) := "\" & C;
         Cursor := Cursor + 2;
      end loop;

      pragma Assert (Cursor = Quoted'Last + 1);

      return Normal & Quoted;
   end Quote_Metacharacters;


   procedure Add_Class
     (To            : in out Class_Descriptor;
      Class_Name    : String;
      Class_Content : Ada.Strings.Maps.Character_Set)
   is
   begin
      To.Classes.Append (Class_Pair'(Content => Class_Content,
                               Name    => To_Unbounded_String (Class_Name)));

      To.Is_Sorted := False;
   end Add_Class;

   procedure Sort (Item : in out Class_Descriptor)
   is
      function "<" (X, Y : Class_Pair) return Boolean
      is (X.Content /= Y.Content and X.Content <= Y.Content);

      type Class_Array is array (Positive range <>) of Class_Pair;



      package Class_Ordering is
        new Partial_Order_Sorting.Array_Sort (Index_Type     => Positive,
                                              Element_Type   => Class_Pair,
                                              Container_Type => class_array);

      Tmp : Class_Array (Item.Classes.First_Index .. Item.Classes.Last_Index);
   begin
      for K in Tmp'Range loop
         Tmp (K) := Item.Classes (K);
      end loop;

      Class_Ordering.Sort (Tmp);

      for K in Tmp'Range loop
         --  Put_Line (To_String (Tmp (K).Name) & ", " &  To_Sequence (Tmp (K).Content));
         Item.Classes (K) := Tmp (K);
      end loop;


      Item.Is_Sorted := True;
   end Sort;


   -------------------
   -- Compact_Range --
   -------------------

   function Compact_Range
     (Charset : String;
      Classes : Class_Descriptor)
      return String
   is
      Set       : Character_Set := To_Set (Charset);
      Compacted : Unbounded_String;
   begin
      for Pair of Classes.Classes loop
         if Pair.Content <= Set then
            Compacted := Compacted & Pair.Name;
            Set := Set and not Pair.Content;
         end if;
      end loop;

      return To_String (Compacted) & To_Sequence (Set);
   end Compact_Range;


   procedure Dump (Item : Class_Descriptor)
   is
   begin
      for Descr of Item.Classes loop
         Put_Line (To_String (Descr.Name));
      end loop;
   end Dump;
end Regexp_Builders.Formatters.Utilities;

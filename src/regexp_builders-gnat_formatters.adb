pragma Ada_2012;

with Ada.Strings.Maps.Constants;
with Regexp_Builders.Formatters.Utilities;

package body Regexp_Builders.Gnat_Formatters is

   Gnat_Classes : Formatters.Utilities.Class_Descriptor;

   function Create return Formatter_Type
   is
      Result : Formatter_Type;
   begin
      return Result;
   end Create;

   ---------
   -- Any --
   ---------

   function Any (Formatter : Formatter_Type;
                 Argument  : Labeled_String;
                 Greedy    : Boolean) return Labeled_String
   is (Argument & (if Greedy then "*" else "*?"));

   ------------------
   -- At_Least_One --
   ------------------

   function At_Least_One
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Greedy    : Boolean) return Labeled_String
   is (Argument & (if Greedy then "+" else "+?"));

   --------------
   -- Optional --
   --------------

   function Optional
     (Formatter : Formatter_Type; Argument : Labeled_String) return Labeled_String
   is (Argument & "?");

   ----------------
   -- Repetition --
   ----------------

   function Repetition
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Min       : Finite_Repetition_Counter;
      Max       : Repetition_Counter)
      return Labeled_String
   is
      pragma Unreferenced (Formatter);
   begin
      if Max = Infinite then
         return Argument
           & "{"
           & Finite_Repetition_Counter'Image (Min)
           & ",}";

      else
         return Argument
           & "{"
           & Finite_Repetition_Counter'Image (Min)
           & ","
           & Finite_Repetition_Counter'Image (Max)
           & "}";

      end if;
   end Repetition;

   -------------
   -- Charset --
   -------------

   function Charset
     (Formatter : Formatter_Type;
      Argument  : String)
      return Labeled_String
   is
      use Formatters.Utilities;
      pragma Unreferenced (Formatter);

      Quoted : constant String := Quote_Metacharacters (Argument, "()[]\$*+?.^-");
   begin
      return To_Labeled("[" & Compact_Range (Quoted, Gnat_Classes) & "]");
   end Charset;


   overriding
   function  Non_Capturing_Group
     (Formatter : Formatter_Type;
      Argument  : Labeled_String)
      return Labeled_String
   is ("(?:" & Argument & ")");

   overriding
   function Capturing_Group
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Fragment  : Fragments.Fragment_Label)
      return Labeled_String
   is
      Result : Labeled_String :=  "(" & Argument & ")";
   begin
      To_Fragment(Result, Fragment);
      return Result;
   end Capturing_Group;
   ----------
   -- Text --
   ----------

   function Text (Formatter : Formatter_Type;
                  Argument  : String)
                  return Labeled_String
   is
      use Formatters.Utilities;

      pragma Unreferenced (Formatter);
      --  function Quote (X : String) return String
      --  is (X);

   begin
      return To_Labeled(Quote_Metacharacters (Argument, "()[]\$*+?.^"));
   end Text;

   ----------------
   -- Whitespace --
   ----------------

   function Whitespace (Formatter : Formatter_Type)
                        return Labeled_String
   is
      pragma Unreferenced (Formatter);
   begin
      return To_Labeled("\s");
   end Whitespace;

begin
   declare
      use Formatters.Utilities;
      use Ada.Strings.Maps;
      use Ada.Strings.Maps.Constants;

      Lower : constant Character_Set := To_Set (Character_Range'('a', 'z'));
      Upper : constant Character_Set := To_Set (Character_Range'('A', 'Z'));
      Alpha : constant Character_Set := Lower or Upper;
      Alnum : constant Character_Set := Alpha or Decimal_Digit_Set;
   begin
      Add_Class (Gnat_Classes, "[:lower:]", Lower);
      Add_Class (Gnat_Classes, "[:digit:]", Decimal_Digit_Set);
      Add_Class (Gnat_Classes, "[:alnum:]", Alnum);
      Add_Class (Gnat_Classes, "[:xdigit:]", Hexadecimal_Digit_Set);
      Add_Class (Gnat_Classes, "[:alpha:]", Alpha);
      Add_Class (Gnat_Classes, "[:upper:]", Upper);

      Sort (Gnat_Classes);
      --  Dump (Gnat_Classes);
   end;
end Regexp_Builders.Gnat_Formatters;

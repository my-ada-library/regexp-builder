package Regexp_Builders is
   type Repetition_Counter is mod 2 ** 32;

   Infinite : constant Repetition_Counter := Repetition_Counter'Last;

   subtype Finite_Repetition_Counter is
     Repetition_Counter range 0 .. Repetition_Counter'Last - 1;


   type Regexp_Style is (Gnat, Perl, Basic_Posix, Extended_Posix);

   type Anchor_Position is (At_Beginning, At_End, Both);
end Regexp_Builders;

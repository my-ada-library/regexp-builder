with Regexp_Builders.Formatters;
with Regexp_Builders.Labeled_Strings;  use Regexp_Builders.Labeled_Strings;
with Regexp_Builders.Fragments;

private package Regexp_Builders.Gnat_Formatters is
   type Formatter_Type is
     new Formatters.Abstract_Formatter with null record;

   function Create return Formatter_Type;

   overriding
   function Any
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Greedy    : Boolean)
      return Labeled_String;

   overriding
   function At_Least_One
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Greedy    : Boolean)
      return Labeled_String;

   overriding
   function Optional
     (Formatter : Formatter_Type;
      Argument  : Labeled_String)
      return Labeled_String;

   overriding
   function Repetition
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Min       : Finite_Repetition_Counter;
      Max       : Repetition_Counter)
      return Labeled_String;

   overriding
   function Charset
     (Formatter : Formatter_Type;
      Argument  : String)
      return Labeled_String;

   overriding
   function  Non_Capturing_Group
     (Formatter : Formatter_Type;
      Argument  : Labeled_String)
      return Labeled_String;

   overriding
   function Capturing_Group
     (Formatter : Formatter_Type;
      Argument  : Labeled_String;
      Fragment  : Fragments.Fragment_Label)
      return Labeled_String;

   overriding
   function Text
     (Formatter : Formatter_Type;
      Argument  : String)
      return Labeled_String;

   overriding
   function Whitespace
     (Formatter : Formatter_Type)
      return Labeled_String;

end Regexp_Builders.Gnat_Formatters;

with Ada.Strings.Maps;              use Ada.Strings.Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;
--
-- This package contains few functions and procedures that can prove
-- useful for writing formatters
--
package Regexp_Builders.Formatters.Utilities is
   --
   -- Split partitions character set Item into two sets:
   -- 1. a set with the elements of Item that are metacharacters
   -- 2. a with the other elements of Item
   --
   procedure Split
     (Item            : Character_Set;
      Metacharacters  : Character_Set;
      Meta_Subset     : out Character_Set;
      Non_Meta_Subset : out Character_Set)
     with Post =>
       ((Meta_Subset or Non_Meta_Subset) = Item)
       and ((Metacharacters and Non_Meta_Subset) = Null_Set)
       and (Meta_Subset <= Metacharacters);

   --
   -- Syntactic sugar that accepts Item and Metacharacters as strings
   --
   procedure Split
     (Item            : String;
      Metacharacters  : String;
      Meta_Subset     : out Character_Set;
      Non_Meta_Subset : out Character_Set);

   --
   -- Call process for every character in Item and return the concatenation
   -- of the results
   --
   function Quote_Set
     (Item    : Character_Set;
      Process : access function (X : Character) return String)
      return String;

   --
   -- Syntactic sugar for a very common case: quote every character by
   -- adding a prefix.
   --
   function Quote_Set
     (Item   : Character_Set;
      Prefix : String)
      return String;

   function Quote_Metacharacters
     (Item           : String;
      Metacharacters : String)
      return String;

   type Class_Descriptor is private;

   procedure Add_Class
     (To            : in out Class_Descriptor;
      Class_Name    : String;
      Class_Content : Ada.Strings.Maps.Character_Set);

   procedure Dump (Item : Class_Descriptor);

   procedure Sort (Item : in out Class_Descriptor);

   function Compact_Range
     (Charset : String;
      Classes : Class_Descriptor)
      return String;
private
   type Class_Pair is
      record
         Content : Ada.Strings.Maps.Character_Set;
         Name    : Ada.Strings.Unbounded.Unbounded_String;
      end record;

   package Class_Pair_Lists is
     new Ada.Containers.Vectors (Index_Type   => Positive,
                                 Element_Type => Class_Pair);

   type Class_Descriptor is
      record
         Is_Sorted : Boolean := False;
         Classes   : Class_Pair_Lists.Vector;
      end record;
end Regexp_Builders.Formatters.Utilities;

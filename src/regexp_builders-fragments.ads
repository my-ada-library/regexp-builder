with Ada.Iterator_Interfaces;
with Ada.Strings.Unbounded;           use Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Maps;

package Regexp_Builders.Fragments is
   --
   --  Fragments are the parts of the regular expression that are usually
   --  within parenthesis (...) (emacs uses \(...\)).  They are used to
   --  extract part of the matched string.  In order to extract the
   --  desired fragment from a matched string it is (usually) necessary to
   --  know the position of the corresponding pair of parenthesis.
   --
   --  For example, the following regular expression matches a pair of numbers
   --  Ada-style (with underscores) separated by a comma and maybe spaces
   --
   --    ([0-9]+(_[0-9]+)*) *, *([0-9]+(_[0-9]+)*)
   --
   --  Depending on the specific regexp implementation the first number with
   --  \1 or $1 or... and the second number with \3 or $3 or ... because
   --  we want the fragment that begins with the third parenthesis.
   --
   --  Note that the second pair of parenthesis are not used as a fragment,
   --  but only to change the precedence of operators and that, most
   --  probably, would be added automatically by this library when
   --  rendering the regexp.  This makes parenthesis counting tricky.
   --
   --  Therefore, we adopted the following solution: when the user desires
   --  to extract a portion of the matching string, it creates a "fragment"
   --  assigning to it a label represented by a "fragment number."
   --  When the regexp will be rendered, the render will create an association
   --  between the fragment numbers and the actual parenthesis number. This
   --  association is returned by the rendering functions as a
   --  `Group_Map` implemented as an array indexed by the fragment labels.s
   --
   type Fragment_Label is private;

   No_Label : constant Fragment_Label;

   function Label(X : String) return Fragment_Label;

   function Image(X:Fragment_Label) return String;

   subtype Group_Index is Integer range 1 .. Integer'Last;

   type Group_Map is tagged private
     with Constant_Indexing => Get;

   type Cursor is private;

   function Has_Element(Position : Cursor) return Boolean;

   function Contains (Map   : Group_Map;
                      Label : Fragment_Label)
                      return Boolean;

   procedure Insert (Map : in out Group_Map;
                     Label : Fragment_Label;
                     Group : Group_Index)
     with
       Pre => Label /= No_Label and then not  Contains(Map, Label),
       Post => Contains(Map, Label) and then Map.get(Label) = Group;

   function Get(Map : Group_Map;
                Label : Fragment_Label)
                return Group_Index
     with
       Pre => Label /= No_Label and then Contains(Map, Label);

   package Map_Iterators is
     new Ada.Iterator_Interfaces(Cursor       => Cursor,
                                 Has_Element  => Has_Element);

   function Iterate(Map : Group_Map)
                    return Map_Iterators.Forward_Iterator'Class;

   function Label (Pos : Cursor) return Fragment_Label
     with
       Pre => Has_Element(Pos);

   function Group (Pos: Cursor) return Group_Index
     with
       Pre => Has_Element(Pos);

private
   type Fragment_Label is new Unbounded_String;

   No_Label : constant Fragment_Label :=
     Fragment_Label(Null_Unbounded_String);

   package Group_Maps is
     new Ada.Containers.Ordered_Maps(Key_Type     => Fragment_Label,
                                     Element_Type => Group_Index);

   type Group_Map is tagged
      record
         Map : Group_Maps.Map;
      end record;

   type Cursor is new Group_Maps.Cursor;

   type Group_Iterator is
     new Map_Iterators.Forward_Iterator
   with
      record
         Start : Cursor;
      end record;

   overriding
   function First (Object : Group_Iterator) return Cursor;

   overriding
   function Next (Object : Group_Iterator; Position : Cursor) return Cursor;
end Regexp_Builders.Fragments;

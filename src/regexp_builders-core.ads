with Ada.Strings.Maps;    use Ada.Strings.Maps;
with Ada.Containers.Indefinite_Multiway_Trees;
with Regexp_Builders.Fragments;

private with Regexp_Builders.Formatters;

--
--  @description
--  This package provides the core functionalities for regexp
--  generation.
--
--  The idea is the following
--
--  - First the user creates a "high-level" regexp descriptor (Regexp_Pattern)
--    using the functions and procedure provided in this package
--
--  - Successively the user produces a string with the desired regexp by
--    "printing" the `Regexp_Pattern` using a `Print` function
--
--  Since different regex implementations have slightly different syntaxes,
--  the `Print` function delegates the details to a `Regexp_Formatter`.
--  Currently only the `Regexp_Formatter` for GNAT.Regpat is implemented
--
--
package Regexp_Builders.Core is
   type Regexp_Pattern is private;

   type Regexp_Formatter is private;

   function Print (X     : Regexp_Pattern;
                   Style : Regexp_Style := Gnat) return String;

   function Print (X      : Regexp_Pattern;
                   Labels : out Fragments.Group_Map;
                   Style  : Regexp_Style := Gnat) return String;


   function Print (X         : Regexp_Pattern;
                   Labels    : out Fragments.Group_Map;
                   Formatter : Regexp_Formatter) return String;

   function Fragment (X     : Regexp_Pattern;
                      Label : Fragments.Fragment_Label) return Regexp_Pattern
     with
       Pre => not Is_Final (X),
       Post => not Is_Final (Fragment'Result);


   function Fragment (X     : Regexp_Pattern) return Regexp_Pattern
     with
       Pre => not Is_Final (X),
       Post => not Is_Final (Fragment'Result);


   ---------------------
   -- Symbolic_Labels --
   ---------------------

   generic
      type Label_Name is (<>);
   package Symbolic_Labels is
      type Name_Map is array (Label_Name) of Fragments.Group_Index;

      function Fragment (X     : Regexp_Pattern;
                         Label : Label_Name) return Regexp_Pattern;

      function Print (X      : Regexp_Pattern;
                      Labels : out Name_Map;
                      Style  : Regexp_Style := Gnat) return String;

      function Print (X         : Regexp_Pattern;
                      Labels    : out Name_Map;
                      Formatter : Regexp_Formatter) return String;
   end Symbolic_Labels;

   function Is_Final (X : Regexp_Pattern) return Boolean;

   --
   --  Return a regexp that match literally X.  Any special characters
   --  (e.g., *, +, ...) are not interpreted but matched as simple
   --  characters
   --
   function Text(X: String) return Regexp_Pattern
     with
       Post => not Is_Final (Text'Result);

   --  Some syntactic sugar
   function T (X : String) return Regexp_Pattern
               renames Text;

   --  Some syntactic sugar
   function Quote (X : String) return Regexp_Pattern
               renames Text;

   --  Some syntactic sugar
   function "+"(X:String) return Regexp_Pattern
                renames Text;

   --
   --  Return a regular expression that matches any character in X
   --  Akin to [...]
   --
   function Charset (X : Character_Set) return Regexp_Pattern
     with
       Post => not Is_Final (Charset'Result);


   --
   --  Return a regular expression that matches any character in X
   --  Akin to [...]
   --
   function Any_Char_From (X : String) return Regexp_Pattern
   is (Charset (To_Set (X)))
     with
       Post => not Is_Final (Any_Char_From'Result);

   --
   --  Return a regular expression that matches any character in
   --  the specified interval. Akin to [A-Z]
   --
   function Interval (Lo, Hi : Character) return Regexp_Pattern
     with
       Post => not Is_Final (Interval'Result);

   -- Regexp patterns named after the POSIX classes
   function Word  return Regexp_Pattern; -- alnum + '_'
   function Alpha return Regexp_Pattern;
   function Alnum return Regexp_Pattern;
   function Lower return Regexp_Pattern;
   function Upper return Regexp_Pattern;
   function Digit return Regexp_Pattern;


   -- The official POSIX name is xdigit, but I prefer the longer version
   function Hexadecimal_Digit return Regexp_Pattern;
   function Xdigit return Regexp_Pattern
     renames Hexadecimal_Digit;

   --
   --  Concatenate two regexp
   --
   function "&" (X, Y : Regexp_Pattern) return Regexp_Pattern
     with
       Pre => not Is_Final (X) and not Is_Final (Y),
     Post => not Is_Final ("&"'Result);

   --
   --  Syntactic sugar when one regexp is a string
   --
   function "&" (X : Regexp_Pattern; Y : String) return Regexp_Pattern;
   function "&" (X : String; Y : Regexp_Pattern) return Regexp_Pattern;

   --
   --  Alternative
   --
   function "or" (X, Y : Regexp_Pattern) return Regexp_Pattern
     with
       Pre => not Is_Final (X) and not Is_Final (Y),
     Post => not Is_Final ("or"'Result);

   --
   --  Matches any char, like '.'
   --
   function Any_Char return Regexp_Pattern
     with
       Post => not Is_Final (Any_Char'Result);


   --
   --  Matches any string like .*
   --
   function Any_String return Regexp_Pattern
     with
       Post => not Is_Final (Any_String'Result);

   --
   --  Matches any whitespace like \s
   --
   function Whitespace return Regexp_Pattern
     with
       Post => not Is_Final (Whitespace'Result);

   --
   --   Matches any number of whitespaces like \s*
   --
   function Any_Space return Regexp_Pattern
     with
       Post => not Is_Final (Any_Space'Result);

   --
   --  Matches any number (even none) of repetition of X, like '*'
   --
   function Any
     (X      : Regexp_Pattern;
      Greedy : Boolean := True) return Regexp_Pattern
     with
       Pre => not Is_Final (X),
       Post => not Is_Final (Any'Result);


   --
   --  Match any number of X, but at least one like '+'
   --
   function At_Least_One
     (X      : Regexp_Pattern;
      Greedy : Boolean := True) return Regexp_Pattern
     with
       Pre => not Is_Final (X),
       Post => not Is_Final (At_Least_One'Result);


   --
   --  Matches zero or one repetitions of X, like ?
   --
   function Maybe
     (X      : Regexp_Pattern;
      Greedy : Boolean := True) return Regexp_Pattern
     with
       Pre => not Is_Final (X),
       Post => not Is_Final (Maybe'Result);

   --  Syntactic sugar
   function Optional
     (X      : Regexp_Pattern;
      Greedy : Boolean := True) return Regexp_Pattern
      renames Maybe;


   --
   --   General repetition function like {n, m}
   --
   function Repetition (X   : Regexp_Pattern;
                        Min : Finite_Repetition_Counter;
                        Max : Repetition_Counter)
                        return Regexp_Pattern
     with
       Pre => not Is_Final (X) and Max >= Min,
       Post => not Is_Final (Repetition'Result);

   --
   --  Anchor the regexp at the beginning, end or both like ^ and $
   --  would do.  Note that the result Is_Final, therefore cannot
   --  be used as parameter of any other function
   --
   function Anchor (What  : Regexp_Pattern;
                    Where : Anchor_Position)
                    return Regexp_Pattern
     with Post => Is_Final (Anchor'Result);




   type Side_Type is (None, Before, After, Both);

   --
   --  Return a regexp that matches a list of one or more Item
   --  separated by Separator.  Spaces can be allowed before or
   --  after each Separator depending on the Allow_Space parameter
   --
   function List_Of (Item        : Regexp_Pattern;
                     Separator   : String;
                     Allow_Space : Side_Type)
                     return Regexp_Pattern
     with
       Pre => not Is_Final (Item),
       Post => not Is_Final (List_Of'Result);


   --
   --  Similar to the other List_Of, but now Separator is a regular
   --  expression (this is why there is no Allow_Space parameter)

   --
   function List_Of (Item      : Regexp_Pattern;
                     Separator : Regexp_Pattern)
                     return Regexp_Pattern
     with
       Pre => not Is_Final (Item) and not Is_Final (Separator),
     Post => not Is_Final (List_Of'Result);


private
   type Regexp_Formatter is access Formatters.Abstract_Formatter'Class;

   --  function Print (X         : Regexp_Pattern;
   --                  Labels    : out Fragments.Group_Map;
   --                  Formatter : Formatters.Abstract_Formatter'Class) return String;

   type Operator is
     (
      Anchor,
      Concat,
      Alternative,
      Any,
      At_Least_One,
      Maybe,
      Repetition,
      Fragment,
      Charset,
      Any_Char,
      Text,
      Whitespace
     );

   subtype Binary_Operator is Operator range Concat .. Alternative;
   subtype Unary_Operator  is Operator range Any .. Maybe;
   subtype True_Operator is Operator range Concat .. Maybe;
   subtype Leaf_Operator is Operator range Charset .. Text;

   type Atom (Op : Operator; Length : Natural) is
      record
         case Op is
            when Anchor =>
               Position : Anchor_Position;

            when Binary_Operator =>
               null;

            when Unary_Operator =>
               Greedy : Boolean;

            when Repetition =>
               Min : Finite_Repetition_Counter;
               Max : Repetition_Counter;

            when Fragment =>
               Label : Fragments.Fragment_Label;

            when Leaf_Operator =>
               Val : String (1 .. Length);

            when Whitespace =>
               null;
         end case;
      end record;

   package Pattern_Trees is
     new Ada.Containers.Indefinite_Multiway_Trees (Atom);

   use type Ada.Containers.Count_Type;

   type Regexp_Pattern is
      record
         T : Pattern_Trees.Tree;
      end record
     with
       Dynamic_Predicate =>
         Pattern_Trees.Child_Count (Regexp_Pattern.T.Root) = 1;
end Regexp_Builders.Core;

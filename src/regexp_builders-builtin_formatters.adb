with Ada.Containers.Indefinite_Holders;
with Regexp_Builders.Gnat_Formatters;

package body Regexp_Builders.Builtin_Formatters is
   use Regexp_Builders.Formatters;

   package Formatter_Holders is
     new Ada.Containers.Indefinite_Holders (Abstract_Formatter'Class);

   use Formatter_Holders;

   Builtin_Formatter_Array : constant array (Regexp_Style) of Formatter_Holders.Holder :=
                               (Gnat           => To_Holder (Gnat_Formatters.Create),
                                Perl           => Formatter_Holders.Empty_Holder,
                                Basic_Posix    => Formatter_Holders.Empty_Holder,
                                Extended_Posix => Formatter_Holders.Empty_Holder);

   function Formatter_For (Style : Regexp_Style) return Abstract_Formatter'Class
   is
   begin
      if Builtin_Formatters.Builtin_Formatter_Array (Style).Is_Empty then
         raise Unimplemented_Builtin_Formatter with Regexp_Style'Image (Style);

      else
         return Builtin_Formatters.Builtin_Formatter_Array (Style).Element;
      end if;
   end Formatter_For;

end Regexp_Builders.Builtin_Formatters;

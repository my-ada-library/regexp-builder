pragma Ada_2012;
package body Regexp_Builders.Fragments is

   -----------
   -- Label --
   -----------

   function Label (X : String) return Fragment_Label is
   begin
      return Fragment_Label'(To_Unbounded_String(X));
   end Label;


   -----------
   -- Image --
   -----------

   function Image(X:Fragment_Label) return String
   is (To_String(X));

   --------------
   -- Contains --
   --------------

   function Contains (Map : Group_Map; Label : Fragment_Label) return Boolean
   is (Map.Map.Contains(Label));

   ------------
   -- Insert --
   ------------

   procedure Insert
     (Map : in out Group_Map; Label : Fragment_Label; Group : Group_Index)
   is
   begin
      if Map.Contains(Label) then
         raise Constraint_Error;
      end if;

      Map.Map.Insert(Key      => Label,
                     New_Item => Group);
   end Insert;

   ---------
   -- Get --
   ---------

   function Get (Map : Group_Map; Label : Fragment_Label) return Group_Index is
   begin
      if not Map.Contains(Label) then
         raise Constraint_Error;
      end if;

      return Map.Map(Label);
   end Get;


   -----------------
   -- Has_Element --
   -----------------

   function Has_Element(Position : Cursor) return Boolean
   is (Group_Maps.Has_Element(Group_Maps.Cursor(Position)));

   -------------
   -- Iterate --
   -------------

   function Iterate(Map : Group_Map)
                    return Map_Iterators.Forward_Iterator'Class
   is
   begin
      return Group_Iterator'(Start => Cursor(Map.Map.First));
   end Iterate;

   -----------
   -- Label --
   -----------

   function Label (Pos : Cursor) return Fragment_Label
   is (Group_Maps.Key(Group_Maps.Cursor(Pos)));

   -----------
   -- Group --
   -----------

   function Group (Pos: Cursor) return Group_Index
   is (Group_Maps.Element(Group_Maps.Cursor(Pos)));

   -----------
   -- First --
   -----------

   function First (Object : Group_Iterator) return Cursor
   is (Object.Start);


   ----------
   -- Next --
   ----------

   function Next (Object : Group_Iterator; Position : Cursor) return Cursor
   is (Cursor(Group_Maps.Next(Group_Maps.Cursor(Position))));

end Regexp_Builders.Fragments;

# Regexp builder 

## What is this? 

This is a simple library to make it easier to create regular expressions.

## Why?

I must confess: I actually *like* regular expressions (yes, I am rare breed... and if you think I code in Ada I am even rarer... :smile:).  I find them very powerful and convenient, but let's be honest: sometimes they look like a cat just walked across your keyboard...  I mean, look at the regular expression for a floating point number

```
[+-\]?[[:digit:]]+\.[[:digit:]]+(?:[Ee][+-\]?[[:digit:]]+)?
```

Even I, every now and then, find the regular expression syntax quite daunting. Therefore, I decided to write this small library that makes it easier to write complex regexps. As an example, the above regular expression can be built as
```ada
   use Regexp_Builders.Core;
   
   Sign         : constant Regexp_Pattern := Any_Char_From ("+-");
   Int_Pattern  : constant Regexp_Pattern := At_Least_One (Digit);
   Exponent     : constant Regexp_Pattern :=
                     Any_Char_From ("eE")  & Maybe (Sign) & Int_Pattern;

   Float_Regexp : constant Regexp_Pattern :=
                    Maybe (Sign) & Int_Pattern & Text (".") & Int_Pattern & Maybe (Exponent);

```
Then you "convert" `Float_Regexp` to a `String` (that can be used with your preferred regexp library) with `Print`; for example, with `GNAT.Regexps`
```ada
   R : constant GNAT.Regexps.Regexp := GNAT.Regexps.Compile (Print (Float_Regexp));
```

A bit more verbose, I agree, but more readable.
